const path = require('path')
const connectToMongo = require(path.resolve('./mongoConn.js'))

// first
function matchPerYear (database) {
  return new Promise((resolve, reject) => {
    connectToMongo.mongoConn(database).then((collections) => {
      collections.matches.aggregate([{
        $group: {
          '_id': '$season',
          'y': {
            $sum: 1
          }
        }
      }]).project({
        _id: 0,
        'name': '$_id',
        'y': 1
      }).toArray(function (err, matchPerYear) {
        if (err) throw err
        collections.conn.close()
        resolve(matchPerYear)
      })
    }).catch(function (err) {
      reject(err)
    })
  })
}

// second
function matchesWonPerYear (database) {
  return new Promise((resolve, reject) => {
    connectToMongo.mongoConn(database).then((collections) => {
      collections.matches.aggregate([{
        $group: {
          '_id': {
            'winner': '$winner',
            'season': '$season'
          },
          'count': {
            $sum: 1
          }
        }
      }, {
        $project: {
          '_id': 0,
          'winner': '$_id.winner',
          'season': '$_id.season',
          count: 1
        }
      }, {
        $group: {
          '_id': '$winner',
          'teamArray': {
            $push: {
              season: '$season',
              count: '$count'
            }
          }
        }

      }]).toArray(function (err, matchesWonPerYear) {
        if (err) throw err
        collections.conn.close()
        resolve(matchesWonPerYear)
      })
    }).catch(function (err) {
      reject(err)
    })
  })
}

// third
function runsConceded (database) {
  return new Promise((resolve, reject) => {
    connectToMongo.mongoConn(database).then((collections) => {
      collections.matches.aggregate([{
        $match: {
          season: 2016
        }
      }, {
        $lookup: {
          from: 'deliveries',
          localField: 'id',
          foreignField: 'match_id',
          as: 'deliveries'
        }
      }, {
        $unwind: '$deliveries'
      }, {
        $group: {
          '_id': '$deliveries.bowling_team',
          'extra_runs': {
            $sum: '$deliveries.extra_runs'
          }
        }
      }, {
        $project: {
          '_id': 0,
          'name': '$_id',
          'y': '$extra_runs'
        }
      }]).toArray(function (err, runsConceded) {
        if (err) throw err
        collections.conn.close()
        resolve(runsConceded)
      })
    }).catch(function (err) {
      reject(err)
    })
  })
}

// fourth
function economicalBowlers (database) {
  return new Promise((resolve, reject) => {
    connectToMongo.mongoConn(database).then((collections) => {
      collections.matches.aggregate([{
        $match: {
          season: 2015
        }
      }, {
        $lookup: {
          from: 'deliveries',
          localField: 'id',
          foreignField: 'match_id',
          as: 'deliveries'
        }
      }, {
        $unwind: '$deliveries'
      }, {
        $group: {
          _id: '$deliveries.bowler',
          Runs: {
            $sum: '$deliveries.total_runs'
          },
          Balls: {
            $sum: 1
          }
        }
      }, {
        $project: {
          _id: 0,
          Bowler: '$_id',
          Balls: '$Balls',
          Runs: '$Runs',
          Economy: {
            $multiply: [{
              $divide: ['$Runs', '$Balls']
            }, 6]
          }
        }
      }]).toArray(function (err, economicalBowlers) {
        if (err) throw err
        collections.conn.close()
        resolve(economicalBowlers)
      })
    }).catch(function (err) {
      reject(err)
    })
  })
}

// fifth
function whoWon (database) {
  return new Promise((resolve, reject) => {
    connectToMongo.mongoConn(database).then((collections) => {
      collections.matches.aggregate([{
        $group: {
          '_id': {
            'winner': '$winner',
            'win_by_runs': '$win_by_runs',
            'year': '$season'
          }
        }
      }, {
        $project: {
          '_id': 0,
          'Winner': '$_id.winner',
          'Win Condition': {
            $cond: [{
              $ne: ['$_id.win_by_runs', 0]
            }, 1, 0]
          },
          'year': '$_id.year'
        }
      }]).toArray(function (err, whoWon) {
        if (err) throw err
        collections.conn.close()
        resolve(whoWon)
      })
    }).catch(function (err) {
      reject(err)
    })
  })
}

module.exports = {
  matchPerYear,
  matchesWonPerYear,
  runsConceded,
  economicalBowlers,
  whoWon
}
