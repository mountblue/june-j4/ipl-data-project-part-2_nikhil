Highcharts.chart('container', {
  'chart': {
    type: 'column'
  },
  'title': {
    text: 'Matches per Year'
  },
  'xAxis': {
    type: 'category',
    title: {
      text: 'Year'
    }
  },
  'yAxis': {
    title: {
      text: 'Number of matches'
    }

  },
  'legend': {
    enabled: false
  },

  'plotOptions': {
    series: {
      dataLabels: {
        enabled: true
      }
    }
  },

  'series': [{
    'name': 'Matches',
    'colorByPoint': true,
    'data': x
  }]
});
