Highcharts.chart('container', {

  chart: {
    type: 'variwide'
  },

  title: {
    text: 'Most Economical Bowlers in 2015'
  },

  yAxis: {
    title: {
      text: 'Economy'
    }
  },

  xAxis: {
    type: 'category',
    title: {
      text: 'Bowlers'
    },
    labels: {
      rotation: -70,
      style: {
        fontSize: '13px',
        fontFamily: 'Verdana, sans-serif'
      }
    }
  },

  legend: {
    enabled: false
  },

  series: [{
    name: 'Economy',
    data: x,
    colorByPoint: true
  }]

})
