
Highcharts.chart('container', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Yearly win records'
  },
  xAxis: {
    categories: x.years,
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Number'
    }
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: x.chartData
})
