Highcharts.chart('container', {
  colors: ['rgb(244, 91, 91)', 'blue', 'rgb(54, 29, 150)', 'pink', 'rgb(144, 237, 125)', 'rgb(43, 144, 143)', 'aqua', 'magenta', 'orange', 'indigo', 'rgb(67, 67, 72)'],
  chart: {
    type: 'bar'
  },
  title: {
    text: 'Number of matches won by each team per year'
  },
  xAxis: {
    categories: [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Number of matches'
    },
    stackLabels: {
      enabled: true,
      style: {
        color: 'gray'
      }
    }
  },
  legend: {
    reversed: true
  },
  plotOptions: {
    series: {
      stacking: 'normal'
    }
  },
  series: x
})
