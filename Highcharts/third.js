Highcharts.chart('container', {
  chart: {
    type: 'pie'
  },
  title: {
    text: 'Extra runs conceded per team for Year 2016'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.y}',
        style: {
          color: 'black'
        }
      }
    }
  },
  series: [{
    name: 'Runs',
    colorByPoint: true,
    data: x
  }]
})
