const expect = require('chai').expect
const path = require('path')
const mongo = require(path.resolve('./mongoOperations'))
const database = 'test'

describe('Mongo Tests\n', function () {
  it('Matches per year of all the years', function (done) {
    let expected = [{
      y: 2,
      name: 2016
    },
    {
      y: 2,
      name: 2015
    },
    {
      y: 1,
      name: 2008
    },
    {
      y: 3,
      name: 2017
    },
    {
      y: 1,
      name: 2009
    },
    {
      y: 1,
      name: 2012
    },
    {
      y: 1,
      name: 2014
    }
    ]

    mongo.matchPerYear(database).then((data) => {
      try {
        expect(data).to.deep.equal(expected)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('Matches won per team per year', function (done) {
    let expected = [{
      '_id': 'Rajasthan Royals',
      'teamArray': [{
        'season': 2015,
        'count': 1
      }]
    }, {
      '_id': 'Rising Pune Supergiants',
      'teamArray': [{
        'season': 2016,
        'count': 1
      }]
    }, {
      '_id': 'Kolkata Knight Riders',
      'teamArray': [{
        'season': 2014,
        'count': 1
      }, {
        'season': 2008,
        'count': 1
      }, {
        'season': 2016,
        'count': 1
      }]
    }, {
      '_id': 'Delhi Daredevils',
      'teamArray': [{
        'season': 2009,
        'count': 1
      }]
    }, {
      '_id': 'Mumbai Indians',
      'teamArray': [{
        'season': 2017,
        'count': 1
      }, {
        'season': 2012,
        'count': 1
      }]
    }, {
      '_id': 'Sunrisers Hyderabad',
      'teamArray': [{
        'season': 2017,
        'count': 1
      }]
    }, {
      '_id': 'Chennai Super Kings',
      'teamArray': [{
        'season': 2015,
        'count': 1
      }]
    }, {
      '_id': 'Royal Challengers Bangalore',
      'teamArray': [{
        'season': 2017,
        'count': 1
      }]
    }]

    mongo.matchesWonPerYear(database).then((data) => {
      try {
        expect(data).to.deep.equal(expected)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('Runs conceded per team in 2015', function (done) {
    let expected = [{
      name: 'Gujarat Lions',
      y: 0
    }, {
      name: 'Kolkata Knight Riders',
      y: 8
    }]

    mongo.runsConceded(database).then((data) => {
      try {
        expect(data).to.deep.equal(expected)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('Most economical Bowlers in 2016', function (done) {
    let expected = [{
      Bowler: 'BCJ Cutting',
      Balls: 1,
      Runs: 0,
      Economy: 0
    },
    {
      Bowler: 'YS Chahal',
      Balls: 1,
      Runs: 0,
      Economy: 0
    },
    {
      Bowler: 'TS Mills',
      Balls: 1,
      Runs: 1,
      Economy: 6
    },
    {
      Bowler: 'B Kumar',
      Balls: 1,
      Runs: 0,
      Economy: 0
    },
    {
      Bowler: 'A Nehra',
      Balls: 1,
      Runs: 0,
      Economy: 0
    }
    ]

    mongo.economicalBowlers(database).then((data) => {
      try {
        expect(data).to.deep.equal(expected)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('Bat/Ball First Win', function (done) {
    let expected = [{
      Winner: 'Kolkata Knight Riders',
      'Win Condition': 0,
      year: 2016
    },
    {
      Winner: 'Sunrisers Hyderabad',
      'Win Condition': 0,
      year: 2017
    },
    {
      Winner: 'Royal Challengers Bangalore',
      'Win Condition': 1,
      year: 2017
    },
    {
      Winner: 'Mumbai Indians',
      'Win Condition': 0,
      year: 2017
    },
    {
      Winner: 'Rajasthan Royals',
      'Win Condition': 1,
      year: 2015
    },
    {
      Winner: 'Rising Pune Supergiants',
      'Win Condition': 0,
      year: 2016
    },
    {
      Winner: 'Delhi Daredevils',
      'Win Condition': 0,
      year: 2009
    },
    {
      Winner: 'Mumbai Indians',
      'Win Condition': 0,
      year: 2012
    },
    {
      Winner: 'Kolkata Knight Riders',
      'Win Condition': 1,
      year: 2008
    },
    {
      Winner: 'Chennai Super Kings',
      'Win Condition': 1,
      year: 2015
    },
    {
      Winner: 'Kolkata Knight Riders',
      'Win Condition': 1,
      year: 2014
    }
    ]

    mongo.whoWon(database).then((data) => {
      try {
        expect(data).to.deep.equal(expected)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
