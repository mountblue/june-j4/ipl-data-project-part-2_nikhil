function mongoConn (database) {
  const MongoClient = require('mongodb').MongoClient
  const url = 'mongodb://localhost:27017'
  return new Promise((resolve, reject) => {
    MongoClient.connect(url, {
      useNewUrlParser: true
    }, function (err, conn) {
      if (err) {
        console.log('WARN: start the mongodb service!')
        reject(err.message)
        // process.exit(0)
      } else {
        // console.log('Connection Successful')
        let dataset = conn.db(database)
        let matches = dataset.collection('matches')
        let deliveries = dataset.collection('deliveries')
        resolve({
          matches,
          deliveries,
          conn
        })
      }
    })
  })
}

module.exports = {
  mongoConn
}
