let express = require('express')
let path = require('path')
let mongo = require(path.resolve('./mongoOperations'))

let app = express()

app.use(express.static('styles'))
app.use(express.static('Highcharts'))
app.set('view engine', 'ejs')

app.get('/', function (req, res) {
  res.render('index')
})

app.get('/first', function (req, res) {
  mongo.matchPerYear('data').then((data) => {
    res.render('first', {
      first: JSON.stringify(data)
    })
  }).catch(function () {
    res.render('error')
  })
})

app.get('/second', function (req, res) {
  mongo.matchesWonPerYear('data').then((data) => {
    let teamWonData = {}
    let seasons = []
    let exportData = []
    for (let i in data) {
      if (data[i].hasOwnProperty('_id')) {
        let teamName = data[i]['_id']
        if (teamName.length > 0) {
          teamWonData[teamName] = {}
          for (let yr = 2008; yr <= 2017; yr++) {
            seasons.push(yr)
            teamWonData[teamName][yr] = 0
          }
          data[i]['teamArray'].forEach(function (yearData) {
            teamWonData[teamName][yearData.season] = yearData.count
          })
        }
      }
    }

    for (let team in teamWonData) {
      exportData.push({
        name: team,
        data: Object.values(teamWonData[team])
      })
    }

    res.render('second', {second: JSON.stringify(exportData)})
  }).catch(function () {
    res.render('error')
  })
})

app.get('/third', function (req, res) {
  mongo.runsConceded('data').then((data) => {
    res.render('third', {
      third: JSON.stringify(data)
    })
    // res.send(data)
  }).catch(function () {
    res.render('error')
  })
})

app.get('/fourth', function (req, res) {
  mongo.economicalBowlers('data').then((data) => {
    let exportData = []
    for (let i of data) {
      exportData.push([i.Bowler, Number((i.Economy).toFixed(2)), ((i.Balls) / 6)])
    }
    exportData.sort(function (a, b) {
      return a[1] - b[1]
    })
    exportData = exportData.splice(0, 10)
    res.render('fourth', {
      fourth: JSON.stringify(exportData)
    })
  }).catch(function () {
    res.render('error')
  })
})

app.get('/fifth', function (req, res) {
  mongo.matchPerYear('data').then((firstData) => {
    mongo.whoWon('data').then((data) => {
      let importData = {}
      let exportData = {}
      for (let match of data) {
        if (importData.hasOwnProperty(match.year)) {
          match['Win Condition'] === 1 ? importData[match.year][0]++ : importData[match.year][1]++
        } else {
          importData[match.year] = [0, 0]
          match['Win Condition'] === 1 ? importData[match.year][0]++ : importData[match.year][1]++
        }
      }

      let years = []
      let runWins = []
      let wicketWins = []

      for (let i in importData) {
        years.push(i)
        runWins.push(importData[i][0])
        wicketWins.push(firstData[0]['y'] - importData[i][0])
      }
      exportData['years'] = years
      exportData['chartData'] = [{
        'name': 'Batting first',
        'data': runWins
      }, {
        'name': 'Bowling first',
        'data': wicketWins
      }]
      res.render('fifth', {
        fifth: JSON.stringify(exportData)
      })
    }).catch(function () {
      res.render('error')
    })
  })
})

app.listen(3000, function () {
  console.log('listening on 3000!')
})